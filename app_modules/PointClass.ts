import Cords2D from './CordsClass';
import Vector2D from './VectorClass';

export default class Point2D {

    private cords: Cords2D;

    constructor(x: number, y: number) {

        this.cords = new Cords2D(x, y);
    }

    // GETTERS AND SETTERS
    public setCords(x: number, y: number): void {

        this.cords.setCordX(x);
        this.cords.setCordY(y);
    }

    public getCords(): Cords2D {

        return this.cords;
    }

    // METHODS
    public getPointTranslatedByVector(vector: Vector2D): Point2D {

        var newPoint = new Point2D(this.cords.getCordX() + vector.getGrowthX(), this.cords.getCordY() + vector.getGrowthY());
        return newPoint;
    }

}
