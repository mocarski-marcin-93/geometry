"use strict";
var Cords2D = (function () {
    function Cords2D(x, y) {
        this.x = x;
        this.y = y;
    }
    Cords2D.prototype.setCordX = function (x) {
        this.x = x;
    };
    Cords2D.prototype.setCordY = function (y) {
        this.y = y;
    };
    Cords2D.prototype.getCordX = function () {
        return this.x;
    };
    Cords2D.prototype.getCordY = function () {
        return this.y;
    };
    return Cords2D;
}());
exports.__esModule = true;
exports["default"] = Cords2D;
