"use strict";
var PointClass_1 = require('./PointClass');
var CordsClass_1 = require('./CordsClass');
var Cartesian2D = (function () {
    function Cartesian2D(x, y) {
        this.map = new Array(x);
        for (var i = 0; i < x; i++) {
            this.map[i] = new Array(y);
        }
        for (var i = 0; i < x; i++) {
            for (var j = 0; j < y; j++) {
                this.map[i][j] = new CordsClass_1.default(i, j);
            }
        }
    }
    Cartesian2D.prototype.addPoint = function (point) {
        var x = point.getCords().getCordX();
        var y = point.getCords().getCordY();
        console.log(this.map.length);
        if (x < this.map.length) {
            if (y < this.map[x].length) {
                if (this.map[x][y]) {
                    this.map[x][y] = point;
                }
            }
        }
    };
    Cartesian2D.prototype.addFigure = function (figure) {
        var _this = this;
        var vertexes = figure.getAllVertexes();
        vertexes.forEach(function (item, index) {
            _this.addPoint(item);
        });
    };
    Cartesian2D.prototype.draw = function () {
        for (var i = 0; i < this.map.length; i++) {
            for (var j = 0; j < this.map[i].length; j++) {
                var point = document.createElement('DIV');
                point.style.height = '1px';
                point.style.width = '1px';
                point.style.position = 'fixed';
                point.style.top = i + 'px';
                point.style.left = j + 'px';
                if (this.map[i][j] instanceof PointClass_1.default) {
                    point.style.background = "yellow";
                }
                else {
                    point.style.background = "blue";
                }
                document.body.appendChild(point);
            }
        }
    };
    return Cartesian2D;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Cartesian2D;
