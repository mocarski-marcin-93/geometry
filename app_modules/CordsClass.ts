export default class Cords2D {

    private x:number;
    private y:number;

    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }

    public setCordX(x: number): void {
        this.x = x;
    }

    public setCordY(y: number): void {
        this.y = y;
    }

    public getCordX(): number {
        return this.x;
    }

    public getCordY():number {
      return this.y;
    }

}
