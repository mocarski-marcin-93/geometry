"use strict";
var CordsClass_1 = require('./CordsClass');
var Point2D = (function () {
    function Point2D(x, y) {
        this.cords = new CordsClass_1.default(x, y);
    }
    Point2D.prototype.setCords = function (x, y) {
        this.cords.setCordX(x);
        this.cords.setCordY(y);
    };
    Point2D.prototype.getCords = function () {
        return this.cords;
    };
    Point2D.prototype.getPointTranslatedByVector = function (vector) {
        var newPoint = new Point2D(this.cords.getCordX() + vector.getGrowthX(), this.cords.getCordY() + vector.getGrowthY());
        return newPoint;
    };
    return Point2D;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Point2D;
