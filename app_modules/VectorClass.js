"use strict";
var Vector2D = (function () {
    function Vector2D(growthX, growthY) {
        this.growthX = growthX;
        this.growthY = growthY;
    }
    Vector2D.prototype.setGrowth = function (growthX, growthY) {
        this.growthX = growthX;
        this.growthY = growthY;
    };
    Vector2D.prototype.getGrowthX = function () {
        return this.growthX;
    };
    Vector2D.prototype.getGrowthY = function () {
        return this.growthY;
    };
    Vector2D.prototype.getStepVector = function () {
        var divisor = 20;
        var newGrowthX = this.growthX / divisor;
        var newGrowthY = this.growthY / divisor;
        var stepVector = new Vector2D(newGrowthX, newGrowthY);
        return stepVector;
    };
    return Vector2D;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Vector2D;
