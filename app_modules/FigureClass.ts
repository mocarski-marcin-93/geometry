import Point2D from './PointClass';
import Vector2D from './VectorClass';

export default class Figure2D {

    private name: string;
    private vertexes: Point2D[];
    private vertexesNumber: number;

    constructor(name: string, vertexes: Point2D[]) {
        this.name = name;
        this.vertexes = vertexes;
        this.vertexesNumber = this.vertexes.length;
        this.makePrintable();
    }

    public setVertex(i: number, vertex: Point2D): void {
        if (this.vertexes[i]) {
            this.vertexes[i] = vertex;
        }
    }

    public getVertex(i: number): Point2D {
        if (this.vertexes[i]) {
            return this.vertexes[i];
        }
    }

    public getAllVertexes(): Point2D[] {
        return this.vertexes;
    }

    public makePrintable(): void {

        let _this = this;
        this.vertexes.forEach(function(item, index) {
            //console.log('makePrintable forEach iteriation ', index);
            var vector: Vector2D;
            var dx = _this.vertexes[(index + 1) % _this.vertexesNumber].getCords().getCordX() - item.getCords().getCordX();
            var dy = _this.vertexes[(index + 1) % _this.vertexesNumber].getCords().getCordY() - item.getCords().getCordY();

            vector = new Vector2D(dx, dy);
            //console.log(vector);
            var stepVector = vector.getStepVector();
            //console.log('to: ', stepVector);
            let i = 0;
            while (i < 20) {
                var point = item.getPointTranslatedByVector(stepVector);
                item = point;
                point = new Point2D(Math.round(point.getCords().getCordX()), Math.round(point.getCords().getCordY()));
                i++;
                _this.vertexes.push(point);
            }
        });
    }

}
