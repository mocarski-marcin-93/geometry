"use strict";
var PointClass_1 = require('./PointClass');
var VectorClass_1 = require('./VectorClass');
var Figure2D = (function () {
    function Figure2D(name, vertexes) {
        this.name = name;
        this.vertexes = vertexes;
        this.vertexesNumber = this.vertexes.length;
        this.makePrintable();
    }
    Figure2D.prototype.setVertex = function (i, vertex) {
        if (this.vertexes[i]) {
            this.vertexes[i] = vertex;
        }
    };
    Figure2D.prototype.getVertex = function (i) {
        if (this.vertexes[i]) {
            return this.vertexes[i];
        }
    };
    Figure2D.prototype.getAllVertexes = function () {
        return this.vertexes;
    };
    Figure2D.prototype.makePrintable = function () {
        var _this = this;
        this.vertexes.forEach(function (item, index) {
            var vector;
            var dx = _this.vertexes[(index + 1) % _this.vertexesNumber].getCords().getCordX() - item.getCords().getCordX();
            var dy = _this.vertexes[(index + 1) % _this.vertexesNumber].getCords().getCordY() - item.getCords().getCordY();
            vector = new VectorClass_1.default(dx, dy);
            var stepVector = vector.getStepVector();
            var i = 0;
            while (i < 20) {
                var point = item.getPointTranslatedByVector(stepVector);
                item = point;
                point = new PointClass_1.default(Math.round(point.getCords().getCordX()), Math.round(point.getCords().getCordY()));
                i++;
                _this.vertexes.push(point);
            }
        });
    };
    return Figure2D;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Figure2D;
