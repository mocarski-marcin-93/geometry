import Point2D from './PointClass';

export default class Vector2D {

    private growthX: number;
    private growthY: number;

    constructor(growthX: number, growthY: number) {
        this.growthX = growthX;
        this.growthY = growthY;
    }

    // GETTERS + SETTERS
    public setGrowth(growthX: number, growthY: number) {
        this.growthX = growthX;
        this.growthY = growthY;
    }

    public getGrowthX(): number {
        return this.growthX;
    }

    public getGrowthY(): number {
        return this.growthY;
    }

    // METHODS
    public getStepVector(): Vector2D {
        var divisor = 20;
        var newGrowthX = this.growthX / divisor;
        var newGrowthY = this.growthY / divisor;
        var stepVector = new Vector2D(newGrowthX, newGrowthY);
        return stepVector;
    }

}
