import Figure2D from './FigureClass';
import Point2D from './PointClass';
import Cords2D from './CordsClass';

export default class Cartesian2D {

    private map: any[][];

    constructor(x: number, y: number) {

        this.map = new Array(x);
        for (let i = 0; i < x; i++) {
            this.map[i] = new Array(y);
        }
        //console.log(this.map);
        for (let i = 0; i < x; i++) {
            for (let j = 0; j < y; j++) {
                this.map[i][j] = new Cords2D(i, j);
            }
        }
    }

    public addPoint(point: Point2D): void {

        var x = point.getCords().getCordX();
        var y = point.getCords().getCordY();
        console.log(this.map.length);
        if (x < this.map.length) {
            if (y < this.map[x].length) {
                if (this.map[x][y]) {
                    this.map[x][y] = point;
                }
            }
        }
    }

    public addFigure(figure: Figure2D): void {
        let _this = this;
        var vertexes = figure.getAllVertexes();
        vertexes.forEach(function(item, index) {
            _this.addPoint(item);

        });
    }

    public draw(): void {

        for (let i = 0; i < this.map.length; i++) {
            for (let j = 0; j < this.map[i].length; j++) {
                var point = document.createElement('DIV');
                point.style.height = '1px';
                point.style.width = '1px';
                point.style.position = 'fixed';
                point.style.top = i + 'px';
                point.style.left = j + 'px';
                if (this.map[i][j] instanceof Point2D) {
                    point.style.background = "yellow";
                } else {
                    point.style.background = "blue";
                }
                document.body.appendChild(point);
            }
        }
    }
}
