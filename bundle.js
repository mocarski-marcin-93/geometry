(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";
var PointClass_1 = require('./PointClass');
var CordsClass_1 = require('./CordsClass');
var Cartesian2D = (function () {
    function Cartesian2D(x, y) {
        this.map = new Array(x);
        for (var i = 0; i < x; i++) {
            this.map[i] = new Array(y);
        }
        //console.log(this.map);
        for (var i = 0; i < x; i++) {
            for (var j = 0; j < y; j++) {
                this.map[i][j] = new CordsClass_1.default(i, j);
            }
        }
    }
    Cartesian2D.prototype.addPoint = function (point) {
        var x = point.getCords().getCordX();
        var y = point.getCords().getCordY();
        console.log(this.map.length);
        if (x < this.map.length) {
            if (y < this.map[x].length) {
                if (this.map[x][y]) {
                    this.map[x][y] = point;
                }
            }
        }
    };
    Cartesian2D.prototype.addFigure = function (figure) {
        var _this = this;
        var vertexes = figure.getAllVertexes();
        vertexes.forEach(function (item, index) {
            _this.addPoint(item);
        });
    };
    Cartesian2D.prototype.draw = function () {
        for (var i = 0; i < this.map.length; i++) {
            for (var j = 0; j < this.map[i].length; j++) {
                var point = document.createElement('DIV');
                point.style.height = '1px';
                point.style.width = '1px';
                point.style.position = 'fixed';
                point.style.top = i + 'px';
                point.style.left = j + 'px';
                if (this.map[i][j] instanceof PointClass_1.default) {
                    point.style.background = "yellow";
                }
                else {
                    point.style.background = "blue";
                }
                document.body.appendChild(point);
            }
        }
    };
    return Cartesian2D;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Cartesian2D;

},{"./CordsClass":2,"./PointClass":4}],2:[function(require,module,exports){
"use strict";
var Cords2D = (function () {
    function Cords2D(x, y) {
        this.x = x;
        this.y = y;
    }
    Cords2D.prototype.setCordX = function (x) {
        this.x = x;
    };
    Cords2D.prototype.setCordY = function (y) {
        this.y = y;
    };
    Cords2D.prototype.getCordX = function () {
        return this.x;
    };
    Cords2D.prototype.getCordY = function () {
        return this.y;
    };
    return Cords2D;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Cords2D;

},{}],3:[function(require,module,exports){
"use strict";
var PointClass_1 = require('./PointClass');
var VectorClass_1 = require('./VectorClass');
var Figure2D = (function () {
    function Figure2D(name, vertexes) {
        this.name = name;
        this.vertexes = vertexes;
        this.vertexesNumber = this.vertexes.length;
        this.makePrintable();
    }
    Figure2D.prototype.setVertex = function (i, vertex) {
        if (this.vertexes[i]) {
            this.vertexes[i] = vertex;
        }
    };
    Figure2D.prototype.getVertex = function (i) {
        if (this.vertexes[i]) {
            return this.vertexes[i];
        }
    };
    Figure2D.prototype.getAllVertexes = function () {
        return this.vertexes;
    };
    Figure2D.prototype.makePrintable = function () {
        var _this = this;
        this.vertexes.forEach(function (item, index) {
            //console.log('makePrintable forEach iteriation ', index);
            var vector;
            var dx = _this.vertexes[(index + 1) % _this.vertexesNumber].getCords().getCordX() - item.getCords().getCordX();
            var dy = _this.vertexes[(index + 1) % _this.vertexesNumber].getCords().getCordY() - item.getCords().getCordY();
            vector = new VectorClass_1.default(dx, dy);
            //console.log(vector);
            var stepVector = vector.getStepVector();
            //console.log('to: ', stepVector);
            var i = 0;
            while (i < 20) {
                var point = item.getPointTranslatedByVector(stepVector);
                item = point;
                point = new PointClass_1.default(Math.round(point.getCords().getCordX()), Math.round(point.getCords().getCordY()));
                i++;
                _this.vertexes.push(point);
            }
        });
    };
    return Figure2D;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Figure2D;

},{"./PointClass":4,"./VectorClass":5}],4:[function(require,module,exports){
"use strict";
var CordsClass_1 = require('./CordsClass');
var Point2D = (function () {
    function Point2D(x, y) {
        this.cords = new CordsClass_1.default(x, y);
    }
    // GETTERS AND SETTERS
    Point2D.prototype.setCords = function (x, y) {
        this.cords.setCordX(x);
        this.cords.setCordY(y);
    };
    Point2D.prototype.getCords = function () {
        return this.cords;
    };
    // METHODS
    Point2D.prototype.getPointTranslatedByVector = function (vector) {
        var newPoint = new Point2D(this.cords.getCordX() + vector.getGrowthX(), this.cords.getCordY() + vector.getGrowthY());
        return newPoint;
    };
    return Point2D;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Point2D;

},{"./CordsClass":2}],5:[function(require,module,exports){
"use strict";
var Vector2D = (function () {
    function Vector2D(growthX, growthY) {
        this.growthX = growthX;
        this.growthY = growthY;
    }
    // GETTERS + SETTERS
    Vector2D.prototype.setGrowth = function (growthX, growthY) {
        this.growthX = growthX;
        this.growthY = growthY;
    };
    Vector2D.prototype.getGrowthX = function () {
        return this.growthX;
    };
    Vector2D.prototype.getGrowthY = function () {
        return this.growthY;
    };
    // METHODS
    Vector2D.prototype.getStepVector = function () {
        var divisor = 20;
        var newGrowthX = this.growthX / divisor;
        var newGrowthY = this.growthY / divisor;
        var stepVector = new Vector2D(newGrowthX, newGrowthY);
        return stepVector;
    };
    return Vector2D;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Vector2D;

},{}],6:[function(require,module,exports){
"use strict";
var PointClass_1 = require('./app_modules/PointClass');
var FigureClass_1 = require('./app_modules/FigureClass');
var CartesianClass_1 = require('./app_modules/CartesianClass');
var map = new CartesianClass_1.default(100, 100);
// var point = new Point2D(50, 50);
var figure = new FigureClass_1.default("Kwadrat", [new PointClass_1.default(10, 10), new PointClass_1.default(10, 20), new PointClass_1.default(20, 20), new PointClass_1.default(20, 10)]);
// map.addPoint(point);
map.addFigure(figure);
map.draw();

},{"./app_modules/CartesianClass":1,"./app_modules/FigureClass":3,"./app_modules/PointClass":4}]},{},[6]);
