"use strict";
var PointClass_1 = require('./app_modules/PointClass');
var FigureClass_1 = require('./app_modules/FigureClass');
var CartesianClass_1 = require('./app_modules/CartesianClass');
var map = new CartesianClass_1.default(100, 100);
var figure = new FigureClass_1.default("Kwadrat", [new PointClass_1.default(10, 10), new PointClass_1.default(10, 20), new PointClass_1.default(20, 20), new PointClass_1.default(20, 10)]);
map.addFigure(figure);
map.draw();
