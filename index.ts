import Point2D from './app_modules/PointClass';
import Figure2D from './app_modules/FigureClass';
import Cartesian2D from './app_modules/CartesianClass';

var map = new Cartesian2D(100, 100);
// var point = new Point2D(50, 50);
var figure = new Figure2D("Kwadrat", [new Point2D(10, 10), new Point2D(10, 20), new Point2D(20, 20), new Point2D(20, 10)]);
// map.addPoint(point);
map.addFigure(figure);
map.draw();
